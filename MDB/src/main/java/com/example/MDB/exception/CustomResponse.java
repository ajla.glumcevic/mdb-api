package com.example.MDB.exception;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.util.HashMap;
import java.util.Map;

public class CustomResponse {
    public static ResponseEntity<Object> createResponse(HttpStatus status, String message, Object data){
        Map<String, Object> json = new HashMap<>();
        json.put("status", status);
        json.put("message", message);
        json.put("data", data);
        return new ResponseEntity<>(json, status);
    }
}
