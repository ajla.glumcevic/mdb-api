//package com.example.MDB.mappers;
//
//import com.example.MDB.dto.UserGetDto;
//import com.example.MDB.dto.UserPostDto;
//import com.example.MDB.model.User;
//import org.mapstruct.Mapper;
//
//import java.util.List;
//
//@Mapper(componentModel = "spring")
//public interface MapStructMapper {
//    UserGetDto userToUserGetDto(User user);
//    List<UserGetDto> usersToUserGetDtos(List<User> users);
//    User userPostDtoToUser(UserPostDto userPostDto);
//}
