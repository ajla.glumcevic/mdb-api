package com.example.MDB.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "users"})
public class Movie {

    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 45)
    private String name;

    @Column(name = "release_date", nullable = true, length = 45)
    private String releaseDate;

    @OneToMany(mappedBy = "favoriteMovie", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<User> users;

    public Movie(){}
    public Movie(Long id) {
        this.id = id;
    }
}
