package com.example.MDB.model;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class User {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "username", nullable = false, length = 45)
    private String username;

    @Column(name = "password", nullable = true, length = 45)
    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "favorite_movie", nullable = true)
    private Movie favoriteMovie;
}
