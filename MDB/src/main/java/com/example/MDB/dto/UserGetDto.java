package com.example.MDB.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserGetDto {
    @JsonProperty("id")
    private int id;

    @NotNull
    @JsonProperty("username")
    private String username;

    @JsonProperty("favoriteMovie")
    private String favoriteMovie;
}
