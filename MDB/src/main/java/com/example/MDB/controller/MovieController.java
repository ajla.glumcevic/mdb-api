package com.example.MDB.controller;

import com.example.MDB.model.Movie;
import com.example.MDB.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/movies")
public class MovieController {
    private MovieService movieService;

    @Autowired
    public MovieController(MovieService movieService) { this.movieService = movieService; }

    @GetMapping()
    public ResponseEntity<Object> getMovies() {
        return movieService.getMovies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> getMovie(@PathVariable(name = "id") Long id) {
       return movieService.getMovieById(id);
    }

    @PostMapping()
    public ResponseEntity<Object> addMovie(@RequestBody Movie movie) {
        return movieService.addMovie(movie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteMovie(@PathVariable(name = "id") Long id) {
        return movieService.deleteMovie(id);
    }
}
