package com.example.MDB.service;

import com.example.MDB.model.Movie;
import org.springframework.http.ResponseEntity;

public interface MovieService {
     ResponseEntity<Object> getMovies();
     ResponseEntity<Object> getMovieById(Long id);
     Movie getMovie(Long id);
     ResponseEntity<Object> addMovie(Movie movie);
     ResponseEntity<Object> deleteMovie(Long id);
}