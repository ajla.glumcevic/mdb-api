package com.example.MDB.service;

import com.example.MDB.dto.UserDto;
import com.example.MDB.dto.UserPostDto;
import com.example.MDB.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

    ResponseEntity<Object> getUsers();
    ResponseEntity<Object> addUser(User user);
    ResponseEntity<Object> deleteUser(Long id);
    ResponseEntity<Object> updateFavoriteMovie(Long userid, Long movieid);
    ResponseEntity<Object> getUserByUsername(String username);
    ResponseEntity<Object> getUserById(Long id);
    ResponseEntity<Object> updateUser(Long id, UserDto userDto);
}
