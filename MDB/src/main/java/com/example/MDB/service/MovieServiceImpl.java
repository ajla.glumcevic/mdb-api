package com.example.MDB.service;
import com.example.MDB.exception.CustomResponse;
import com.example.MDB.model.Movie;
import com.example.MDB.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public ResponseEntity<Object> getMovies() {
       List<Movie> movies = movieRepository.findAll();
       return CustomResponse.createResponse(HttpStatus.OK, "", movies);
    }

    @Override
    public ResponseEntity<Object> getMovieById(Long id) {
        Optional<Movie> optionalMovie = movieRepository.findById(id);
        if(optionalMovie.isPresent()){
            return CustomResponse.createResponse(HttpStatus.OK, "", optionalMovie.get());
        }else{
            return CustomResponse.createResponse(HttpStatus.OK, "Movie doesn't exist in database.", null);
        }
    }

    @Override
    public Movie getMovie(Long id) {
        Optional<Movie> optionalMovie = movieRepository.findById(id);
        return optionalMovie.orElse(null);
    }

    @Override
    public ResponseEntity<Object> addMovie(Movie movie) {
            try {
                if (movieRepository.findByName(movie.getName()).isEmpty()) {
                    movieRepository.save(movie);
                    System.out.println(movie.getReleaseDate());
                    return CustomResponse.createResponse(HttpStatus.OK, "New movie added.", null);
                } else {
                    return CustomResponse.createResponse(HttpStatus.OK, "Movie already exists.", null);
                }
            } catch (Exception e) {
                return CustomResponse.createResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), null);
            }
    }

    @Override
    public ResponseEntity<Object> deleteMovie(Long id) {
        try {
            if (movieRepository.existsById(id)) {
                movieRepository.deleteById(id);
                return CustomResponse.createResponse(HttpStatus.OK, "Movie deleted.", null);
            }else{
                return CustomResponse.createResponse(HttpStatus.OK, "Movie doesn't exist in database.", null);
            }
        }catch (Exception e){
            return CustomResponse.createResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), null);
        }
    }
}
