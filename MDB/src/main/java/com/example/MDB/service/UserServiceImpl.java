package com.example.MDB.service;

import com.example.MDB.dto.UserDto;
import com.example.MDB.exception.CustomResponse;
import com.example.MDB.model.Movie;
import com.example.MDB.model.User;
import com.example.MDB.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MovieService movieService;


    private String getSha(String password) throws Exception {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(password.getBytes("utf8"));
            return String.format("%040x", new BigInteger(1, digest.digest()));
    }

    @Override
    public ResponseEntity<Object> getUsers() {
        List<User> users = userRepository.findAll();
        return CustomResponse.createResponse(HttpStatus.OK, "", users);
    }

    @Override
    public ResponseEntity<Object> addUser(User user) {
        try {
            user.getUsername();
            if (userRepository.findByUsername(user.getUsername()).isEmpty()) {
                user.setPassword(getSha(user.getPassword()));
                userRepository.save(user);
                return CustomResponse.createResponse(HttpStatus.OK, "New user added.", null);
            }
            return CustomResponse.createResponse(HttpStatus.OK, "Username already exists.", null);
        } catch (Exception e) {
            return CustomResponse.createResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), null);
        }
    }

    @Override
    public ResponseEntity<Object> deleteUser(Long id) {
        try {
            if (userRepository.existsById(id)) {
                userRepository.deleteById(id);
                return CustomResponse.createResponse(HttpStatus.OK, "User deleted.", null);
            }
            return CustomResponse.createResponse(HttpStatus.OK, "User doesn't exist in database.", null);
        }catch (Exception e){
            return CustomResponse.createResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), null);
        }
    }

    @Override
    public ResponseEntity<Object> updateFavoriteMovie(Long userid, Long movieid) {
        try{
            Movie movie = movieService.getMovie(movieid);
            if(movie == null) {
                return CustomResponse.createResponse(HttpStatus.OK, "Movie doesn't exist in database.", null);
            }
           if(userRepository.existsById(userid)){
               User user = userRepository.getById(userid);
               user.setFavoriteMovie(movie);
               userRepository.save(user);
               return CustomResponse.createResponse(HttpStatus.OK, "Favorite movie changed.", null);
           }
           return CustomResponse.createResponse(HttpStatus.OK, "User doesn't exist in database.", null);
        }catch (Exception e){
            return CustomResponse.createResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), null);
        }
    }
    @Override
    public ResponseEntity<Object> getUserById(Long id) {
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return CustomResponse.createResponse(HttpStatus.OK, "", user);
        }
        return CustomResponse.createResponse(HttpStatus.OK, "User doesn't exist in database.", null);
    }

    @Override
    public ResponseEntity<Object> updateUser(Long id, UserDto userDto) {
        User user = userRepository.findById(id).orElse(null);
        if(user != null) {
            user.setUsername(userDto.getUsername());
            user.setFavoriteMovie(movieService.getMovie(userDto.getFavoriteMovie()));
            return CustomResponse.createResponse(HttpStatus.OK, "", user);
        }
        return CustomResponse.createResponse(HttpStatus.OK, "User doesn't exist in database.", null);
    }

    @Override
    public ResponseEntity<Object> getUserByUsername(String username) {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if(optionalUser.isPresent()){
            User user = optionalUser.get();
            return CustomResponse.createResponse(HttpStatus.OK, "",  user);
        }
        return CustomResponse.createResponse(HttpStatus.OK, "User doesn't exist in database.", null);
    }

}
