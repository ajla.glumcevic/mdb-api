package com.example.MDB.mappers;

import com.example.MDB.dto.UserGetDto;
import com.example.MDB.dto.UserPostDto;
import com.example.MDB.model.User;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-02-18T23:53:51+0100",
    comments = "version: 1.3.1.Final, compiler: javac, environment: Java 16.0.1 (Oracle Corporation)"
)
@Component
public class MapStructMapperImpl implements MapStructMapper {

    @Override
    public UserGetDto userToUserGetDto(User user) {
        if ( user == null ) {
            return null;
        }

        UserGetDto userGetDto = new UserGetDto();

        return userGetDto;
    }

    @Override
    public List<UserGetDto> usersToUserGetDtos(List<User> users) {
        if ( users == null ) {
            return null;
        }

        List<UserGetDto> list = new ArrayList<UserGetDto>( users.size() );
        for ( User user : users ) {
            list.add( userToUserGetDto( user ) );
        }

        return list;
    }

    @Override
    public User userPostDtoToUser(UserPostDto userPostDto) {
        if ( userPostDto == null ) {
            return null;
        }

        User user = new User();

        return user;
    }
}
